import React from 'react';
import CityListItem from '../city-list-item';
import './city-list.css';

const CityList = ({cities, onDeleted, labelChange}) => {
    const elements = cities.map((city) => {
            const {id, ...itemProps} = city; // id не будет передаваться в массиве itemProps

            return (
                // обязателен id
                <li key={id} className="list-group-item">
                    <CityListItem {...itemProps}
                                  onDeleted={() => onDeleted(id)}
                                  labelChange={labelChange}

                    />
                </li>
            )
        }
    )

    return (
        <ul className="list-group city-list">
            {elements}
        </ul>
    )
}

export default CityList;
