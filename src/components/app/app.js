import React, {Component} from 'react';
import SearchPanel from "../search";
import ErrorIndicator from "../error-indicator";
import CityList from "../city-list";
import ShowWeather from "../show-weather/show-weather";
import './app.css';

export default class App extends Component {
    maxId = 1;

    state = {
        cities: [],
        hasError: false,
        label: ''
    }

    componentDidCatch() {
        this.setState({
            hasError: true
        });
    }

    addCity = (text) => {
        const duplicate = this.duplicate(text);

        if (!duplicate) {
            const newCity = this.createNewCity(text);

            this.setState(({cities}) => {
                    const newArray = [...cities, newCity];
                    return {
                        cities: newArray
                    }
                }
            )
        }
    }

    duplicate(value) {
        const {cities} = this.state;

        for (let i = 0; i < cities.length; i++) {
            if (cities[i].label === value.trim()) return true;
        }

        return false;
    }

    createNewCity(label) {
        return {
            label,
            id: this.maxId++
        }
    }

    deleteCity = (id) => {
        this.setState(({cities}) => {
            const idx = cities.findIndex((el) => el.id === id)

            const before = cities.slice(0, idx); // получаем элементы с 0 до idx
            const after = cities.slice(idx + 1); // получаем элементы с idx до конца
            const newArray = [...before, ...after];

            return {
                cities: newArray
            }
        })
    }

    labelChange = (value) => {
        this.setState({
            label: value
        });
    }

    render() {
        const {cities, hasError, label} = this.state;

        if (hasError) {
            return <ErrorIndicator/>
        }

        return (
            <div className="main-app">
                <h2 className="header">Weather app</h2>
                <div className="app">
                    <div className="div-block-left">
                        <SearchPanel className="top-panel d-flex"
                                     onAdd={this.addCity}
                                     labelChange={this.labelChange}/>
                        <CityList className="top-panel d-flex"
                                  cities={cities}
                                  onDeleted={this.deleteCity}
                                  labelChange={this.labelChange}
                        />
                    </div>
                    <div className="div-block-right">
                        <ShowWeather activeCity={label}/>
                    </div>
                </div>
            </div>
        )
    }
}
