import React, {Component} from 'react';
import './search.css';

export default class SearchPanel extends Component {
    state = {
        label: ''
    }

    onLabelChange = (event) => {
        event.preventDefault();
        const label = event.target.value;
        this.setState({
            label
        })
    }

    onSendLabelChange = (event) => {
        event.preventDefault();
        this.props.labelChange(this.state.label);
    }

    onAdd = (event) => {
        event.preventDefault(); // браузер не будет перезагружать страницу (перерисовывать)
        this.props.onAdd(this.state.label);
    }

    getCurrentLocation = async (lat, lon) => {
        const _urlOpenstreetmap = 'https://nominatim.openstreetmap.org/reverse?format=jsonv2';

        const res = await fetch(`${_urlOpenstreetmap}&lat=${lat}&lon=${lon}`);
        const body = res.json();

        body.then((body) => {
            this.setState({label: body.address.city})
            this.props.labelChange(body.address.city);
        });
    }

    getLocation = () => {
        const getSuccess = (position) => {
            this.getCurrentLocation(position.coords.latitude, position.coords.longitude);
        }

        const getError = (error) => {
            throw error;
        }

        navigator.geolocation.getCurrentPosition(getSuccess, getError);
    }

    render() {
        return (
            <div>
                <form className="search-input d-flex">
                    <input type="text"
                           onChange={this.onLabelChange}
                           onKeyDown={event => event.key === 'Enter' && this.onSendLabelChange(event)}
                           placeholder="Введите название города"
                           value={this.state.label}
                    />
                    <button type="button"
                            className="btn btn-outline-info"
                            onClick={this.onSendLabelChange}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             className="bi bi-search" viewBox="0 0 16 16">
                            <path fillRule="evenodd"
                                  d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                            <path fillRule="evenodd"
                                  d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                        </svg>
                    </button>
                    <button type="button"
                            className="btn btn-outline-info"
                            onClick={this.onAdd}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             className="bi bi-heart" viewBox="0 0 16 16">
                            <path fillRule="evenodd"
                                  d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                        </svg>
                    </button>

                </form>
                <button type="button"
                        className="btn btn-link"
                        onClick={this.getLocation}>
                    Определить мое местоположение
                </button>
            </div>
        )
    }
}
