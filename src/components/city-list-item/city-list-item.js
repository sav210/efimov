import React, {Component} from 'react';
import './city-list-item.css';

export default class CityListItem extends Component {

    choiceCity = (event, label) => {
        event.preventDefault();
        this.props.labelChange(label);
    }

    render() {
        const {label, onDeleted} = this.props;

        return (
            <span className='list-item'>
                <span className="list-item-label"
                      onClickCapture={event => this.choiceCity(event, label)}>
                    {label}
                </span>
                <button type="button" className="btn btn-outline-danger btn-sm float-right"
                        onClick={onDeleted}>
                    <i className="fa fa-trash-o"/>
                </button>
            </span>
        )
    }
}
