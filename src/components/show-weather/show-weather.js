import React, {Component} from 'react';
import './show-weather.css';
import ErrorIndicator from "../error-indicator";

export default class ShowWeather extends Component {
    state = {
        curCity: '',
        weatherData: null,
        error: false,
        mesError: ''
    }

    componentDidMount() {
        const {activeCity} = this.props;

        if (activeCity !== '') this.getWeather(activeCity);
    }

    componentDidUpdate(prevProps) {
        const {activeCity} = this.props;

        if (prevProps.activeCity !== activeCity) this.getWeather(activeCity);
    }

    getWeather = async (nameCity) => {
        const apiKey = 'ccd9b98496a328816772df90417152af';
        const url = 'http://api.openweathermap.org/data/2.5/weather?q=';

        this.setState({
            curCity: nameCity
        })

        if (nameCity !== '') {
            await fetch(`${url}${nameCity}&appid=${apiKey}&lang=ru&units=metric`)
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw response; // вызвать исключение
                    }
                })
                .then((body) => {
                    this.setState({
                        weatherData: body,
                        curCity: nameCity,
                        error: false,
                        mesError: ''
                    })
                })
                .catch(error => {
                    this.setState({
                        error: true,
                        mesError: `Данные не были получены. Ошибка ${error.status}: ${error.statusText}`
                    })
                })
        } else {
            this.setState({
                error: false,
                mesError: ''
            })
        }
    }

    render() {
        const {weatherData, curCity, error, mesError} = this.state;

        if (error) {
            return <ErrorIndicator mesError={mesError}/>
        }

        if (curCity === '' || weatherData === null ) return <h5>Введите город в строку поиска или выберите из списка избранных</h5>;

        const weather = weatherData.weather[0];
        const idIcon = weather.icon;
        const urlPNG = 'http://openweathermap.org/img/wn/';
        const iconWeather = `${urlPNG}${idIcon}@2x.png`

        return (
            <div className="div">
                <span className="text">{weatherData.name}</span>
                <div className="divTemp">
                    <span className="textTemp">{weatherData.main.temp.toFixed(1)}</span>
                    <img alt='' src={iconWeather}/>
                </div>
                <div className="divWeather">
                    <div align="center">
                        <span className="textFeeling">По ощущению {weatherData.main.feels_like.toFixed(1)}</span>
                    </div>
                    <span className="text">{weather.description}</span>
                </div>

                <table align="center">
                    <thead>
                    <tr>
                        <td>
                            <div className="divTd">Ветер</div>
                            <div className="divValue">{weatherData.wind.speed} м/с</div>
                        </td>
                        <td>
                            <div className="divTd">Давление</div>
                            <div className="divValue">{(weatherData.main.pressure * 0.750063755419211).toFixed(0)} мм
                                рт. ст.
                            </div>
                        </td>
                        <td>
                            <div className="divTd">Влажность</div>
                            <div className="divValue">{weatherData.main.humidity} %</div>
                        </td>
                        <td>
                            <div className="divTd">Облачность</div>
                            <div className="divValue">{weatherData.clouds.all} %</div>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        )
    }
}